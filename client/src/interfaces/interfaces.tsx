interface ValutaData {
  from: string;
  to: string;
  amount: number;
}

interface ExchangeRate {
  from: string;
  to: string;
  rate: number;
}

interface InputInterface {
  onChangeInput: (e: any) => void;
  label: string;
  value?: number;
  options?: string[];
  name: string;
}

//TODO () => void should be more specific
interface DialogInterface {
  open: boolean;
  handleToClose: () => void;
  handleToSave?: () => void;
}

export type { InputInterface, ValutaData, DialogInterface, ExchangeRate };
