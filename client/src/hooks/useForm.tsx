import { useState } from "react";
import { ValutaData, ExchangeRate } from "../interfaces";

export const useForm = (initialState: ValutaData | ExchangeRate) => {
  const [data, setData] = useState(initialState);

  const onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setData({ ...data, [name]: value });
  };

  const onSubmitForm = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
  };

  return {
    data,
    onChangeInput,
    onSubmitForm,
  };
};
