import React, { FC } from "react";
import { InputInterface } from "../interfaces";

export const SelectInput: FC<InputInterface> = ({
  onChangeInput,
  options,
  label,
  name,
}) => {
  const renderOptions = () => {
    if (!options && options?.length === 0) return;
    return options?.map((choice) => (
      <option value={choice} key={choice}>
        {choice}
      </option>
    ));
  };

  return (
    <div className="input">
      <label>{label}</label>
      <select name={name} id={name} onChange={(event) => onChangeInput(event)}>
        {renderOptions()}
      </select>
    </div>
  );
};
