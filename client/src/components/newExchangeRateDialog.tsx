import React, { FC } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { DialogInterface } from "../interfaces";
import { NumberInput } from "./numberInput";
import { useForm } from "../hooks";
import { symbols } from "../data/mockData";
import { SelectInput } from ".";

export const NewExchangeRateDialog: FC<DialogInterface> = ({
  handleToClose,
  open,
}) => {
  const { data: exhangeRate, onChangeInput, onSubmitForm } = useForm({
    from: "",
    to: "",
    rate: 330,
  });

  return (
    <Dialog open={open} onClose={handleToClose}>
      <DialogTitle>Új átváltási ráta felvétele</DialogTitle>
      <DialogContent></DialogContent>
      <DialogActions>
        <form>
          <SelectInput
            onChangeInput={onChangeInput}
            label="Erről a valutáról"
            name="from"
            options={symbols}
          />
          <SelectInput
            onChangeInput={onChangeInput}
            label="Erre a valutára"
            name="to"
            options={symbols}
          />
          <NumberInput
            onChangeInput={onChangeInput}
            label="Ráta"
            name="amount"
          />
        </form>

        <button onClick={onSubmitForm}>Mentés</button>
        <button onClick={handleToClose}>Close</button>
      </DialogActions>
    </Dialog>
  );
};
