import React, { FC } from "react";
import { InputInterface } from "../interfaces";

export const NumberInput: FC<InputInterface> = ({
  onChangeInput,
  label,
  name,
}) => {
  return (
    <div className="input">
      <label>{label}</label>
      <input
        type="number"
        onChange={(event) => onChangeInput(event)}
        key={name}
        name={name}
      />
    </div>
  );
};
