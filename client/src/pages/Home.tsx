import React, { useEffect, FC, useContext, useState } from "react";
import "./Home.css";
import { NumberInput, SelectInput, NewExchangeRateDialog } from "../components";
import { useForm } from "../hooks";
import { symbols } from "../data/mockData";

export const HomePage: FC = () => {
  const { data: valuta, onChangeInput, onSubmitForm } = useForm({
    from: "",
    to: "",
    amount: 0,
  });

  const [open, setOpen] = useState(false);

  const handleClickToOpen = () => {
    setOpen(true);
  };

  const handleToClose = () => {
    setOpen(false);
  };

  return (
    <div className="container">
      <h1>Online valutaváltó</h1>
      <form>
        <NumberInput
          onChangeInput={onChangeInput}
          label="Összeg"
          name="amount"
        />
        <SelectInput
          onChangeInput={onChangeInput}
          label="Erről a valutáról"
          name="from"
          options={symbols}
        />
        <SelectInput
          onChangeInput={onChangeInput}
          label="Erre a valutára"
          name="to"
          options={symbols}
        />
      </form>
      <button onClick={onSubmitForm}>Mehet</button>
      <button onClick={handleClickToOpen}>Új váltási ráta felvétele</button>
      <NewExchangeRateDialog handleToClose={handleToClose} open={open} />
    </div>
  );
};
